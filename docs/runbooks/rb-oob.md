# Using OOB Configuration

## Requirements

* Supported Operating System
* Splunk Enterprise or Splunk Cloud

## Splunk Cloud
1. Open a TA deployment request for Splunk_TA_nix
2. Utilize Services or Admin on Demand to patch TA

## Splunk Enterprise Cluster Master

1. Download latest `Splunk_TA_nix-*_indexers.tar.gz`
2. Splunk Cluster Master unzip to master-apps
3. Copy `default/indexes.conf` to `local/indexes.conf`
4. Review homePath and coldPath and redefine as required

## Splunk Enterprise Indexers and Intemediate Forwarders
1. Download latest `Splunk_TA_nix-*_indexers.tar.gz`
2. Login (sudo) to the splunk user `sudo su - splunk`
3. Install the app `splunk app install <filename>`
4. Define custom indexes as neded
4. Restart splunk `systemctl restart splunk`

## Splunk Enterprise Search Head
1. Download latest `Splunk_TA_nix-5.2.4-_search_heads.tar.gz`
2. Install the app `splunk app install <filename>`

## Splunk Forwarders Generic
1. Download and Deploy `Splunk_TA_nix-*.tar.gz`
2. Download `Splunk_TA_nix-5.2.4-_forwarders.tar.gz`
3. Customize and deploy inputs.conf as needed
