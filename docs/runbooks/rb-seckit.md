# Using SecKit Configuration

## Requirements

* Supported Operating System
* Splunk Enterprise or Splunk Cloud

## Planning

[Review Planning Guide](rb-seckit-planning.md)

## Onboarding

[Review onboarding Guide](rb-seckit-onboarding.md)


## Post Install Validation

1. Run the following search or derivative based on deployment scope

```| tstats count where index=osnix* by host```
