## Local Logging Policy/Standard and Retention

###General

The following planning and preparation activities should be conducted in this phase
1. Review inputs.conf file(s) and confirm the location of files in the running environment matches expectations
2. Review configuration of the operating syslog daemon to ensure a consistent syslog configuration is applied to all in scope systems
3. Deploy the Universal Forwarder to all inscope hosts verify connectivity to the deployment and indexers by searching `index=_internal phoneHome | stats latest(_time) as _time by host` and comparing the output list of hosts to the hosts in scope
4. Verify log rotation of each monitored file is configured to rotate one (1) or more times per day retaining at least two (2) prior files using a numeric suffix i.e ".1", ".2". Using a "date" coded suffix can lead to out of disk conditions unless additional customer developed precautions are taken  


### Debian Ubuntu

Default configuration for Debian derived operating systems does not enable the use of /var/log/messages this is commonly enabled for IT operations and Security use increases

#### Create a file `/etc/rsyslog.d/50-messages.conf` ensure the file owner is root file group is root and permissions are -rw-r--r--


	```
	*.=info;*.=notice;*.=warn;\
	        auth,authpriv.none;\
	        cron,daemon.none;\
	        mail,news.none          -/var/log/messages
	        ```


#### Review the configuration of logrotated `/etc/logrotate.d/rsyslog`

* consider updating the period from weekly to daily
* consider reduction of copied from 4 to 3
* consider remove BOTH compress and delay compress to reduce the use of CPU at rotation time


## Capacity Planning

* Measure the average of the size of two full days of log data from all or a sample of hosts. Using customer provided business trends ensure adequate capacity is available to accommodate seasonal or other temporary increases in throughput/storage
* Add 5 MB per host per day for scripted and file change inputs
